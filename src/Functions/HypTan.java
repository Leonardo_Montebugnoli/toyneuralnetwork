package Functions;

public class HypTan extends ActivationFunction {
    @Override
    public float getDVal(float x) {
        return (float) (1-Math.pow(Math.tanh(x),2));
    }

    @Override
    public float getValue(float val) {
        return (float) Math.tanh(val);

    }
}
