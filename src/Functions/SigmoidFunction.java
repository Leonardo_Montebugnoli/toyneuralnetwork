package Functions;

public class SigmoidFunction extends ActivationFunction {
    @Override
    public float getDVal(float x) {
        //return getValue(x) * (1-getValue(x));
        return x * (1-x);
    }

    @Override
    public float getValue(float val) {
        return (float) (1 / (1 + Math.exp(-val)));
    }
}
