import Functions.ActivationFunction;
import Functions.SigmoidFunction;

import java.util.ArrayList;

public class NeuralNetwork {
    private int n_inputs,n_hiddens,n_outputs;
    private Matrix weights_ih,weights_ho;
    private Matrix bias_h,bias_o;

    private ActivationFunction activationFunction;
    private float learning_rate;


    public NeuralNetwork(int n_inputs, int n_hiddens, int n_outputs) {
        this.n_inputs = n_inputs;
        this.n_hiddens = n_hiddens;
        this.n_outputs = n_outputs;

        weights_ih = new Matrix(n_hiddens,n_inputs);
        weights_ho = new Matrix(n_outputs,n_hiddens);

        weights_ih.randomize();
        weights_ho.randomize();

        bias_h = new Matrix(n_hiddens,1);
        bias_o = new Matrix(n_outputs,1);

        bias_h.randomize();
        bias_o.randomize();

        setLearning_rate(0.1f);

        setActivationFunction(new SigmoidFunction());
    }

    public ActivationFunction getActivationFunction() {
        return activationFunction;
    }

    public float getLearning_rate() {
        return learning_rate;
    }

    public void setActivationFunction(ActivationFunction activationFunction) {
        this.activationFunction = activationFunction;
    }

    public void setLearning_rate(float learning_rate) {
        this.learning_rate = learning_rate;
    }


    public ArrayList<Float> predict(ArrayList<Float> inputs_arr,boolean soft){
        Matrix inputs = Matrix.fromArray(inputs_arr);
        Matrix hidden = Matrix.multiply(this.weights_ih, inputs);
        hidden.add(this.bias_h);
        // activation function!
        hidden.map(this.activationFunction, true);

        // Generating the output's output!
        Matrix output = Matrix.multiply(this.weights_ho, hidden);
        output.add(this.bias_o);
        output.map(this.activationFunction, true);


        ArrayList<Float> out = output.toArray();
        if(soft) {
            out = softmax(out);
        }
        return out;
    }


    public void train(ArrayList<Float> input_array, ArrayList<Float> target_array, boolean soft){
        // Generating the Hidden Outputs
        Matrix inputs = Matrix.fromArray(input_array);
        Matrix hidden = Matrix.multiply(this.weights_ih, inputs);
        hidden.add(this.bias_h);
        // activation function!
        hidden.map(this.activationFunction,true);

        // Generating the output's output!
        Matrix outputs = Matrix.multiply(this.weights_ho, hidden);
        outputs.add(this.bias_o);
        outputs.map(this.activationFunction,true);

        // Convert array to matrix object
        Matrix targets = Matrix.fromArray(target_array);

        if(soft) {

            ArrayList<Float> out = outputs.toArray();
            out = softmax(out);

            outputs = Matrix.fromArray(out);
        }
        // Calculate the error
        // ERROR = TARGETS - OUTPUTS
        Matrix output_errors = Matrix.subtract(targets, outputs);

        // let gradient = outputs * (1 - outputs);
        // Calculate gradient
        Matrix gradients = Matrix.map(outputs, this.activationFunction,false);
        gradients.multiply(output_errors);
        gradients.multiply(this.learning_rate);


        // Calculate deltas
        Matrix hidden_T = Matrix.transpose(hidden);
        Matrix weight_ho_deltas = Matrix.multiply(gradients, hidden_T);

        // Adjust the weights by deltas
        this.weights_ho.add(weight_ho_deltas);
        // Adjust the bias by its deltas (which is just the gradients)
        this.bias_o.add(gradients);

        // Calculate the hidden layer errors
        Matrix who_t = Matrix.transpose(this.weights_ho);
        Matrix hidden_errors = Matrix.multiply(who_t, output_errors);

        // Calculate hidden gradient
        Matrix hidden_gradient = Matrix.map(hidden, this.activationFunction,false);
        hidden_gradient.multiply(hidden_errors);
        hidden_gradient.multiply(this.learning_rate);

        // Calcuate input->hidden deltas
        Matrix inputs_T = Matrix.transpose(inputs);
        Matrix weight_ih_deltas = Matrix.multiply(hidden_gradient, inputs_T);

        this.weights_ih.add(weight_ih_deltas);
        // Adjust the bias by its deltas (which is just the gradients)
        this.bias_h.add(hidden_gradient);

    }


    /*public void train(ArrayList<Float> input_array, ArrayList<Float> target_array, boolean soft) {

        Matrix inputs = Matrix.fromArray(input_array);
        Matrix hidden = Matrix.multiply(this.weights_ih, inputs);
        hidden.add(this.bias_h);
        // activation function!
        hidden.map(this.activationFunction, true);

        // Generating the output's output!
        Matrix outputs = Matrix.multiply(this.weights_ho, hidden);
        outputs.add(this.bias_o);
        outputs.map(this.activationFunction, true);


        ArrayList<Float> out = outputs.toArray();
        if(soft) {
            out = softmax(out);
            outputs = Matrix.fromArray(out);
        }
        Matrix targets = Matrix.fromArray(target_array);

        //Calculate error
        Matrix out_errors = Matrix.subtract(targets,outputs);

        Matrix gradient = Matrix.map(outputs,activationFunction,false);
        gradient.multiply(out_errors);
        gradient.multiply(learning_rate);


        //Calculate hidden layer errors
        Matrix hidden_t = Matrix.transpose(hidden);
        Matrix weights_ho_deltas = Matrix.multiply(gradient,hidden_t);

        this.weights_ho.add(weights_ho_deltas);
        this.bias_o.add(gradient);


        Matrix who_t = Matrix.transpose(weights_ho);
        Matrix hidden_errors = Matrix.multiply(who_t,out_errors);

        Matrix hiddenGradients = Matrix.map(hidden,activationFunction,false);

        hiddenGradients.multiply(hidden_errors);
        hiddenGradients.multiply(learning_rate);



        Matrix inputs_t = Matrix.transpose(inputs);
        Matrix weights_ih_deltas = Matrix.multiply(hiddenGradients,inputs_t);

        this.weights_ih.add(weights_ih_deltas);
        this.bias_h.add(hiddenGradients);

    }*/

    private ArrayList<Float> softmax(ArrayList<Float> output){
        float sum = 0;
        ArrayList<Float> out = new ArrayList<>();

        for (int i = 0; i < output.size(); i++) {
            sum += Math.exp(output.get(i));
        }
        for (int i = 0; i < output.size(); i++) {
            float val = (float) Math.exp(output.get(i));
            out.add(val/sum);
        }
        return out;
    }
}
