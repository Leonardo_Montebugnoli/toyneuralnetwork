import Functions.ActivationFunction;

import java.util.ArrayList;
import java.util.Random;

public class Matrix {

    private int rows, col;
    private float[][] matrix;

    public Matrix(int rows, int col) {
        this.rows = rows;
        this.col = col;

        matrix = new float[rows][col];

    }

    //Non static
    private void setZeros(){
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < col; c++) {
                matrix[r][c] = 0f;
            }
        }
    }

    public void setEye(){
        int max = rows<=col ? rows:col;
        for (int i = 0; i < max; i++) {
                matrix[i][i] = 1;
        }
    }


    public void randomize(){
        Random random = new Random();
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < col; c++) {
                matrix[r][c] = random.nextFloat();
            }
        }
    }


    public void randomize(float max){
        Random random = new Random();
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < col; c++) {
                matrix[r][c] = random.nextFloat()*max;
            }
        }
    }

    public ArrayList<Float> toArray(){
        ArrayList<Float> arr = new ArrayList<>();

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < col; c++) {
                arr.add(matrix[r][c]);
            }
        }
        return arr;

    }


    public void add(Matrix m2) throws IllegalArgumentException{
        if(!(this.rows == m2.getRows() && this.col == m2.getCol())){
            throw  new IllegalArgumentException("matrix size must concide");
        }else{

            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < col; c++) {
                    this.matrix[r][c] += m2.getVal(r,c);
                }
            }



        }
    }

    public void multiply(float n){
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < col; c++) {
                this.matrix[r][c] *= n;
            }
        }
    }


    public Matrix copy(){
        Matrix m = new Matrix(rows,col);
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < col; c++) {
                m.setVal(r,c,this.matrix[r][c]);
            }
        }
        return m;
    }


    public float getVal(int r, int c){
        return matrix[r][c];
    }

    public void setVal(int r, int c, float val){
        matrix[r][c] = val;
    }

    public int getRows(){
        return rows;
    }

    public int getCol(){
        return col;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();


        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < col; c++) {
                sb.append(matrix[r][c] + " ");
            }
            sb.append("\n");
        }
        return sb.toString();

    }
    public Matrix map(ActivationFunction f, boolean isVal) {
        if(isVal) {
            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < col; c++) {
                    matrix[r][c] = f.getValue(matrix[r][c]);
                }
            }
        }else{

            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < col; c++) {
                    matrix[r][c] = f.getDVal(matrix[r][c]);
                }
            }
        }
        return this;

    }


    public void multiply(Matrix m2) {
        if(!(rows == m2.getRows() && col == m2.getCol())){
            throw  new IllegalArgumentException("matrix size must concide");
        }else{
            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < col; c++) {
                    matrix[r][c] *= m2.getVal(r,c);
                }
            }
        }
    }


    //Static
    public static Matrix subtract(Matrix m1, Matrix m2) throws IllegalArgumentException{
        if(!(m1.getRows() == m2.getRows() && m1.getCol() == m2.getCol())){
            throw  new IllegalArgumentException("matrix size must concide");
        }else{
            Matrix m = new Matrix(m1.getRows(),m1.getCol());

            for (int r = 0; r < m1.getRows(); r++) {
                for (int c = 0; c < m1.getCol(); c++) {
                    m.setVal(r,c,m1.getVal(r,c)-m2.getVal(r,c));
                }
            }

            return m;
            
            
        }
    }

    public static Matrix multiply(Matrix m1, Matrix m2) throws IllegalArgumentException{
        if(!(m1.getCol() == m2.getRows())){
            throw  new IllegalArgumentException("matrix size must concide");
        }else{
            Matrix m = new Matrix(m1.getRows(),m2.getCol());
            for (int i = 0; i < m1.getRows(); i++) {
                for (int j = 0; j < m2.getCol(); j++) {
                    float sum = 0;

                    for (int k = 0;  k < m2.getRows(); k++) {
                        sum += m1.getVal(i,k) * m2.getVal(k,j);
                    }
                    m.setVal(i,j,sum);
                }

            }

            return m;


        }
    }


    public static Matrix multiply(Matrix m1, float n){
            Matrix m = m1.copy();

            m.multiply(n);

            return m;

    }

    public static Matrix transpose(Matrix m1){

        Matrix m = new Matrix(m1.getCol(),m1.getRows());

        for (int r = 0; r < m.getRows(); r++) {
            for (int c = 0; c < m.getCol(); c++) {
                m.setVal(r,c,m1.getVal(c,r));
            }
        }

        return m;

    }


    public static Matrix fromArray(ArrayList<Float> arr){

            Matrix m = new Matrix(arr.size(),1);

            for (int r = 0; r < m.getRows(); r++) {
                    m.setVal(r,0,arr.get(r));
            }

            return m;
    }


    public static Matrix map(Matrix m1, ActivationFunction f,boolean isVal) {
            Matrix m = new Matrix(m1.getRows(),m1.getCol());

            if(isVal) {
                for (int r = 0; r < m.getRows(); r++) {
                    for (int c = 0; c < m.getCol(); c++) {
                        m.setVal(r, c, f.getValue(m1.getVal(r, c)));
                    }
                }
            }else{

                for (int r = 0; r < m.getRows(); r++) {
                    for (int c = 0; c < m.getCol(); c++) {
                        m.setVal(r, c, f.getDVal(m1.getVal(r, c)));
                    }
                }
            }

            return m;



    }

}
