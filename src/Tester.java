import java.util.ArrayList;
import java.util.Random;

public class Tester {
    public static void main(String[] args) {
        NeuralNetwork nn = new NeuralNetwork(2,2,1);

        ArrayList<Float> inputs00 = new ArrayList<>();
        ArrayList<Float> inputs01 = new ArrayList<>();
        ArrayList<Float> inputs10 = new ArrayList<>();
        ArrayList<Float> inputs11 = new ArrayList<>();


        ArrayList<Float> out00 = new ArrayList<>();
        ArrayList<Float> out01 = new ArrayList<>();
        ArrayList<Float> out10 = new ArrayList<>();
        ArrayList<Float> out11 = new ArrayList<>();




        inputs00.add(0f);
        inputs00.add(0f);

        inputs01.add(0f);
        inputs01.add(1f);

        inputs10.add(1f);
        inputs10.add(0f);

        inputs11.add(1f);
        inputs11.add(1f);


        out00.add(0f);
        out01.add(1f);
        out10.add(1f);
        out11.add(0f);


        ArrayList<ArrayList<Float>> inputs = new ArrayList<>();
        inputs.add(inputs00);
        inputs.add(inputs01);
        inputs.add(inputs10);
        inputs.add(inputs11);


        ArrayList<ArrayList<Float>> outputs = new ArrayList<>();
        outputs.add(out00);
        outputs.add(out01);
        outputs.add(out10);
        outputs.add(out11);


        Random r = new Random();
        for (int i = 0; i < 100000; i++) {
            int val = r.nextInt(4);
            //System.out.println(inputs.get(val) + "  :  "  + outputs.get(val));

            nn.train(inputs.get(val),outputs.get(val),false);
        }


        System.out.println(Math.round(nn.predict(inputs00,false).get(0)));
        System.out.println(Math.round(nn.predict(inputs01,false).get(0)));
        System.out.println(Math.round(nn.predict(inputs10,false).get(0)));
        System.out.println(Math.round(nn.predict(inputs11,false).get(0)));






    }
}
